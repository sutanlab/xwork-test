const express = require('express')
const parser = require('body-parser')
const mongo = require('mongoose')
const routes = require('./src/routes')

const app = express()

async function startServer() {
    try {
        await mongo.connect('mongodb://127.0.0.1:27017/xworktest', {
            useNewUrlParser: true
        })
        app.use(parser.urlencoded({ extended: true }))
        app.use(parser.json())
        app.use(routes)
        app.use(express.static(`${__dirname}/public`))
        app.listen(7700)
        console.log('server running at http://localhost:7700')
    } catch (err) {
        console.log('an error occurred: ', err.message)
    }
}

startServer()