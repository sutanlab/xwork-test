const { Router } = require('express')
const Music = require('../models/Music')

const route = Router()

function httpCatch(res, err) {
    console.log('err: ', err.message)
    res.send({
        status: err.status || 'Error',
        code: err.code || 500,
        message: err.code ? err.message : 'Internal Server Error!'
    })
}

route.get('/api/music', async (req, res) => {
    try {
        const musics = await Music.find().select('title artist album year')
        res.send({
            status: 'Success',
            code: 200,
            data: musics
        })
    } catch (err) {
        httpCatch(res, err)
    }
})

route.get('/api/music/lyric/:id', async (req, res) => {
    try {
        const lyric = await Music.findById(req.params.id).select('lyric')
        if (!lyric) {
            throw {
                status: 'Not Found',
                code: 404,
                message: `Music with id: ${req.params.id} not found!`
            }
        }
        res.send({
            status: 'Success',
            code: 200,
            data: lyric
        })
    } catch (err) {
        httpCatch(res, err)
    }
})

route.post('/api/music', async (req, res) => {
    try {
        if (!req.body.title) {
            throw {
                status: 'Bad Request',
                code: 400,
                message: 'Title field must not be empty!'
            }
        }
        const newMusic = new Music({
            title: req.body.title,
            album: req.body.album || 'Unknown album',
            artist: req.body.artist || 'Unknown artist',
            year: req.body.year || 'Unkown year',
            lyric: req.body.lyric || 'No lyric available'
        })
        const music = await newMusic.save()
        res.send({
            status: 'Success',
            code: 200,
            message: 'Add music success',
            data: music
        })
    } catch (err) {
        httpCatch(res, err)
    }
})

route.delete('/api/music/:id', async (req, res) => {
    try {
        const music = await Music.findByIdAndDelete(req.params.id)
        if (!music) {
            throw {
                status: 'Not Found',
                code: 404,
                message: `Music with id: ${req.params.id} not found!`
            }
        }
        res.send({
            status: 'Success',
            code: 200,
            message: 'Delete music success'
        })
    } catch (err) {
        httpCatch(res, err)
    }
})

module.exports = route