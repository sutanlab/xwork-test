const music = () => ({
    title: '',
    album: '',
    artist: '',
    year: '',
    lyric: '',
})

new Vue({
    el: '#root',
    data: {
        music: music(),
        musics: []
    },
    methods: {
        addNewMusic() {
            window.fetch('/api/music', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(this.music)
            })
                .then(res => res.json())
                .then(({ message, data }) => {
                    delete data.lyric
                    this.musics.push(data)
                    this.music = music()
                    document.getElementById('close-modal').click()
                    window.alert(message)
                })
        },
        loadLyric(id, idx) {
            if (!this.musics[idx].lyric) {
                window.fetch(`/api/music/lyric/${id}`)
                    .then(res => res.json())
                    .then(({ data }) => {
                        this.$set(this.musics[idx], 'lyric', data.lyric)
                    })
            }
        },
        deleteMusic(id, idx) {
            if (window.confirm('Are you sure want to delete this music ?')) {
                window.fetch(`/api/music/${id}`, { method: 'DELETE' })
                    .then(res => res.json())
                    .then(({ message }) => {
                        this.musics.splice(idx, 1)
                        window.alert(message)
                    })
            }
        }
    },
    mounted() {
        M.Modal.init(document.querySelectorAll('.modal'))
        window.fetch('/api/music')
            .then(res => res.json())
            .then(({ data }) => {
                console.log(data)
                this.musics = data
            })
    }
})